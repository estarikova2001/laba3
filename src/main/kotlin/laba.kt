package org.example.laba3

/**
 * Add additions.
 *
 * @param a1 First addition.
 * @param a2 Second first addition.
 * @return a1 + a2
 */

fun add(a1: Int, a2: Int) = a1 + a2
